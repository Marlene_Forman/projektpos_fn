insert into user (id, username, password) values (1, 'user1', 'max1');
insert into user (id, username, password) values (2, 'user2', 'max2');
insert into user (id, username, password) values (3, 'user3', 'max3');
insert into user (id, username, password) values (4, 'user4', 'max4');
insert into user (id, username, password) values (5, 'user5', 'max5');
insert into user (id, username, password) values (6, 'user6', 'max6');

insert into entry (id, text, user) values (1, 'text1', 1);
insert into entry (id, text, user) values (2, 'text2', 2);
insert into entry (id, text, user) values (3, 'text3', 3);
insert into entry (id, text, user) values (4, 'text4', 4);
insert into entry (id, text, user) values (5, 'text5', 5);
insert into entry (id, text, user) values (6, 'text6', 6);