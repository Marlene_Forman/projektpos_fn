package at.journal;

import java.sql.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import at.spenger.web.db.User;
import db.*;


@Controller
public class UserController {
	 
	 private final UserService userService;
	 private Entry entries;
	 

	 @Autowired
	 public UserController(UserService userService) {
		 this.userService = userService;
	 }
	 
	 
	 
	@RequestMapping(value="/login/{id}", method=RequestMethod.GET)
	public String login(@RequestParam("username") String username, @RequestParam("password") String password) {
		for (User s : userService.findAll()){
			if(username == s.getPassword() && password == s.getPassword()){
				return "redirect:/home/";
			}else{
				return "redirect:/login";
			}
		}
		return null;
		
		
	}
	
	@RequestMapping(value="/login/add", method=RequestMethod.GET)
	public String addUser(@RequestParam("username") String username, @RequestParam("password") String password) {
	
		int id = 1;
		for(User s : userService.findAll()){
			id++;
		}
		
		if(username != null && password != null && !username.isEmpty() && !password.isEmpty()) {
			for(User u : userService.findAll()){
				if(u.username == username){
					return null;
				}else{
					User user = new User(username, password);
					userService.save(user);
				}
			}
		}
		
		return "redirect:/login";
	}

	@RequestMapping(value="/entry/add", method=RequestMethod.GET)
	public String addEntry(@RequestParam("username") String UserName, @RequestParam("password") String password) {
	
		/*int id = 1;
		for(User s : userService.findAll())
		{
			id++;
		}
		
		if(UserName != null && password != null && !UserName.isEmpty() && !password.isEmpty()) {
			User user = new User(firstName, lastName);
			user.setBirthdate(birthdate);
			userService.save(user);
		}
		*/
		return "redirect:/entries";
	}
	
	@RequestMapping(value="/entry/{id}/del", method=RequestMethod.GET)
	public String deleteEntry(@PathVariable int id) {
		
		entries.delete(id);
		
		return "redirect:/entry";
	}

}
