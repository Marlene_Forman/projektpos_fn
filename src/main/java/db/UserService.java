package db;

import java.util.List;


public interface UserService{
	Iterable<User> findAll();

	User save(User entity);

	User find(int id);
	
}

	